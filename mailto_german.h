/* General page layout definitions */

#define	TITLE	"WWW Mail Gateway"

/* We need a default form */

#define	DEFAULT_FORM_HEADER	"<FORM ACTION=\"%s\" METHOD=POST>\n"
#define	DEFAULT_FORM_ADDRESS	"<STRONG>Empf&auml;nger-Adresse:</STRONG> <INPUT NAME=\"%s\" SIZE=42><BR>\n"
#define	DEFAULT_FORM_CC	"<STRONG>Kopie an:</STRONG> <INPUT NAME=\"%s\" SIZE=42><BR>\n"
#define	DEFAULT_FORM_SUBJECT	"<STRONG>Betreff:</STRONG> <INPUT NAME=\"%s\" SIZE=54><BR>\n"
#define	DEFAULT_FORM_HIDDEN	"<INPUT TYPE=hidden NAME=\"%s\" VALUE=\"%s\">\n"
#define	DEFAULT_FORM_TRAILER	"<STRONG>Absender-Adresse:</STRONG> <INPUT NAME=\"Reply-To\" SIZE=44><BR>\n\
<STRONG>Nachricht:</STRONG><TEXTAREA NAME=\"Body\" COLS=60 ROWS=9></TEXTAREA><BR>\n\
<INPUT TYPE=submit VALUE=\"Abschicken\">\n\
<INPUT TYPE=reset VALUE=\"L&ouml;schen\">\n\
</FORM>\n"

/* Everything's fine */

#define	SUCCESS_HEADER	NULL
#define	SUCCESS_DESC	"Ihre Meldung wurde abgeschickt.\n"
#define	SUCCESS_SENT	"Ihre Meldung wurde an %s"
#define	SUCCESS_ALSO	", %s"
#define	SUCCESS_COPY	" und eine Kopie an %s abgeschickt.\n"
#define	SUCCESS_FROM	"<P><CODE>From:</CODE> %s\n"
#define	SUCCESS_SUBJECT	"<BR><CODE>Subject:</CODE> %s\n"
#define	SUCCESS_SENDER	"<BR><CODE>Sender:</CODE> %s\n"
#define	SUCCESS_TO	"<BR><CODE>To:</CODE> %s\n"
#define	SUCCESS_CC	"<BR><CODE>Cc:</CODE> %s\n"
#define	SUCCESS_BCC	"<BR><CODE>Bcc:</CODE> %s\n"
#define	SUCCESS_REPLYTO	"<BR><CODE>Reply-To:</CODE> %s\n"
#define	SUCCESS_ADDR	"<BR><CODE>X-Addr:</CODE> %s\n"
#define	SUCCESS_HOST	"<BR><CODE>X-Host:</CODE> %s\n"
#define	SUCCESS_IDENT	"<BR><CODE>X-Ident:</CODE> %s\n"
#define	SUCCESS_USER	"<BR><CODE>X-User:</CODE> %s\n"
#define	SUCCESS_SINGLE	"<P><CODE>%s:</CODE> %s\n"
#define	SUCCESS_MULTI	"<P><CODE>%s:</CODE>\n"
#define	SUCCESS_LINE	"<BR>%s\n"

/* Error messages */

#define	ERROR	"Fehler"
#define	ERROR_FATAL	"<P>Ein solcher Fehler darf eigentlich nicht auftreten. Bitte setzen Sie sich mit <EM>%s</EM> in Verbindung und erl&auml;utern Sie, wie es zu diesem Fehler gekommen ist. Interessant ist insbesondere, von welcher Seite Sie diese Fehlermeldung erhalten, was Sie eingegeben haben und welche Software Sie verwenden."
#define	ERROR_REQUEST_METHOD	"Es wurde ein falscher REQUEST_METHOD-Aufruf (%s) verwendet, erwartet wurde %s."
#define	ERROR_CONTENT_TYPE	"Es wurde ein falscher CONTENT_TYPE (%s) &uuml;bergeben, erwartet wurde %s."
#define	ERROR_CONTENT_LENGTH	"CONTENT_LENGTH ist null."
#define	ERROR_ADDRESS	"Es fehlen die Angaben &uuml;ber Mail-Adresse und Subject."
#define	ERROR_READ	"Fehler beim Lesen der Adressdatei."
#define	ERROR_POPEN	"Fehler beim Verschicken der Meldung:<P><CODE>popen(\"%s\"): %s</CODE>"
#define	ERROR_PCLOSE	"Fehler beim Verschicken der Meldung:<P><CODE>%s</CODE> liefert exit code %d."
#define	BAD_ADDRESS	"Die angegebene Adresse %s ist nicht in der Liste der erlaubten Adressen eingetragen. Um die Benutzer vor anonymen Mails zu sch&uuml;tzen, m&uuml;ssen potentielle Empf&auml;nger sich (via Email an %s) in diese Liste eintragen lassen."
